package com.pio.pdf.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.pio.pdf.common.Constant;
import com.pio.pdf.helper.HeaderAndFooterPdfPageEventHelper;
import com.pio.pdf.model.Product;
import com.pio.pdf.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Product Service Implementation
 *
 * @auther programmers.io
 * Created By: TararamGoyal
 * Date: 15-11-2021
 * Time: 17:12
 * Project Name: pdf
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository repository;

    private static int total = 0;

    public static int getTotal() {
        return total;
    }

    public static void setTotal(int total) {
        ProductServiceImpl.total = total;
    }

    @Override
    public ByteArrayInputStream generateCFDPdf() throws DocumentException, IOException {

        List<Product> productList = repository.findByIdLessThan(500);

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, out);

        Rectangle rectangle = new Rectangle(800, 600);
        pdfWriter.setBoxSize("rectangle", rectangle);
        document.setPageSize(rectangle);

        //set footer
        HeaderAndFooterPdfPageEventHelper headerAndFooter = new HeaderAndFooterPdfPageEventHelper();
        HeaderAndFooterPdfPageEventHelper.setTotalPages(getTotal());
        pdfWriter.setPageEvent(headerAndFooter);

        document.open();

        BaseFont titleFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
        Font headerFont = new Font(titleFont, 20, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font subTitleFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font tableHead = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font data = new Font(Font.FontFamily.HELVETICA, 10);

        document.add(new Paragraph(Constant.TITLE, headerFont));
        document.add(new Paragraph(Constant.CFD_SUB_TITLE, subTitleFont));

        final int NUM_COLUMNS = 7;
        final int NUM_SUB_COLUMNS = 7;

        PdfPTable dataTable = new PdfPTable(NUM_COLUMNS);
        dataTable.setWidthPercentage(100);
        dataTable.setWidths(new int[]{2, 1, 1, 1, 1, 1, 1});
        dataTable.getDefaultCell().setBorder(1);
        dataTable.setHeaderRows(2);


        PdfPTable headTable = new PdfPTable(7);
        headTable.setWidthPercentage(100);
        headTable.setWidths(new int[]{2, 1, 1, 1, 1, 1, 1});
        headTable.getDefaultCell().setBorder(0);

        PdfPCell[] headTableCells = new PdfPCell[NUM_SUB_COLUMNS];

        for (int i = 0; i < NUM_SUB_COLUMNS; i++) {

            headTableCells[i] = new PdfPCell(new Paragraph(Constant.CFD_COLUMN_SUB_HEADINGS[i], tableHead));
            headTableCells[i].setBorder(PdfPCell.NO_BORDER);
            headTableCells[i].setPadding(1);
            headTableCells[i].setHorizontalAlignment(Element.ALIGN_CENTER);
            dataTable.addCell(headTableCells[i]);
        }

        document.add(headTable);

        PdfPCell[] datacells = new PdfPCell[NUM_COLUMNS];

        for (int i = 0; i < NUM_COLUMNS; i++) {
            datacells[i] = new PdfPCell(new Paragraph(Constant.CFD_COLUMN_HEADINGS[i], tableHead));
            datacells[i].setBorder(PdfPCell.BOTTOM);
            datacells[i].setBorderWidthBottom(2f);
            datacells[i].setBorderColor(new BaseColor(23, 12, 128));
            datacells[i].setPadding(8);
            if (i == 0)
                datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
            else
                datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);

            dataTable.addCell(datacells[i]);
        }

        for (Product product : productList) {
            datacells[0] = new PdfPCell(new Paragraph((product.getPrice() * product.getQuantity()) + "" + (product.getPrice() * product.getQuantity()), data));
            datacells[1] = new PdfPCell(new Paragraph(product.getName(), data));
            datacells[2] = new PdfPCell(new Paragraph(product.getPrice() + "", data));
            datacells[3] = new PdfPCell(new Paragraph(product.getQuantity() + "", data));
            datacells[4] = new PdfPCell(new Paragraph(product.getPrice() + "", data));
            datacells[5] = new PdfPCell(new Paragraph(product.getQuantity() + "", data));
            datacells[6] = new PdfPCell(new Paragraph((product.getPrice() * product.getQuantity()) + "", data));

            for (int i = 0; i < NUM_COLUMNS; i++) {
                datacells[i].setBorder(PdfPCell.BOTTOM);
                datacells[i].setPadding(8);
                if (i == 0)
                    datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
                else
                    datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);
                dataTable.addCell(datacells[i]);
            }


        }
        document.add(dataTable);
        document.close();
        return new ByteArrayInputStream(out.toByteArray());

    }

    @Override
    public ByteArrayInputStream generateIFDPdf() throws DocumentException, IOException {

        List<Product> productList = repository.findByIdLessThan(500);

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, out);

        Rectangle rectangle = new Rectangle(800, 600);
        pdfWriter.setBoxSize("rectangle", rectangle);
        document.setPageSize(rectangle);

        //Set footer
        HeaderAndFooterPdfPageEventHelper headerAndFooter = new HeaderAndFooterPdfPageEventHelper();
        HeaderAndFooterPdfPageEventHelper.setTotalPages(getTotal());
        pdfWriter.setPageEvent(headerAndFooter);


        document.open();


        BaseFont titleFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
        Font headerFont = new Font(titleFont, 20, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font subscriptFont = new Font(Font.FontFamily.HELVETICA, 10, Font.ITALIC, new BaseColor(23, 15, 128));
        Font tableHead = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font data = new Font(Font.FontFamily.HELVETICA, 10);

        document.add(new Paragraph(Constant.TITLE, headerFont));
        document.add(new Paragraph(Constant.IFD_SUB_TITLE, subscriptFont));

        final int NUM_COLUMNS = 5;
        int NUM_SUB_COLUMNS = 5;
        float[] columnWidths = {2, 1, 1, 1, 1};


        PdfPTable dataTable = new PdfPTable(NUM_COLUMNS);
        dataTable.setWidthPercentage(100);
        dataTable.setWidths(columnWidths);
        dataTable.getDefaultCell().setBorder(1);
        dataTable.setHeaderRows(2);


        PdfPTable headTable = new PdfPTable(5);
        headTable.setWidthPercentage(100);
        headTable.setWidths(new int[]{2, 1, 1, 1, 1});
        headTable.getDefaultCell().setBorder(0);

        PdfPCell[] headTableCells = new PdfPCell[NUM_SUB_COLUMNS];

        for (int i = 0; i < NUM_SUB_COLUMNS; i++) {
            headTableCells[i] = new PdfPCell(new Paragraph(Constant.IFD_COLUMN_SUB_HEADINGS[i], tableHead));
            headTableCells[i].setBorder(PdfPCell.NO_BORDER);
            headTableCells[i].setPadding(1);
            headTableCells[i].setHorizontalAlignment(Element.ALIGN_CENTER);
            dataTable.addCell(headTableCells[i]);
        }

        document.add(headTable);

        PdfPCell[] datacells = new PdfPCell[NUM_COLUMNS];

        for (int i = 0; i < NUM_COLUMNS; i++) {
            datacells[i] = new PdfPCell(new Paragraph(Constant.IFD_COLUMN_HEADINGS[i], tableHead));
            datacells[i].setBorder(PdfPCell.BOTTOM);
            datacells[i].setBorderWidthBottom(2f);
            datacells[i].setBorderColor(new BaseColor(23, 12, 128));
            datacells[i].setPadding(8);

            if (i == 0)
                datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
            else
                datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);

            dataTable.addCell(datacells[i]);
        }

        for (Product product : productList) {
            datacells[0] = new PdfPCell(new Paragraph((product.getPrice() * product.getQuantity()) + "" + (product.getPrice() * product.getQuantity()), data));
            datacells[1] = new PdfPCell(new Paragraph(product.getName(), data));
            datacells[2] = new PdfPCell(new Paragraph(product.getPrice() + "", data));
            datacells[3] = new PdfPCell(new Paragraph(product.getQuantity() + "", data));
            datacells[4] = new PdfPCell(new Paragraph((product.getPrice() * product.getQuantity()) + "", data));

            for (int i = 0; i < NUM_COLUMNS; i++) {
                datacells[i].setBorder(PdfPCell.BOTTOM);
                datacells[i].setPadding(8);
                if (i == 0)
                    datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
                else
                    datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);
                dataTable.addCell(datacells[i]);
            }


        }
        document.add(dataTable);
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }

    @Override
    public ByteArrayInputStream generateDensityPdf() throws DocumentException, IOException {
        List<Product> productList = repository.findByIdLessThan(500);

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, out);

        Rectangle rectangle = new Rectangle(800, 600);
        pdfWriter.setBoxSize("rectangle", rectangle);
        document.setPageSize(rectangle);

        //Set footer
        HeaderAndFooterPdfPageEventHelper headerAndFooter = new HeaderAndFooterPdfPageEventHelper();
        HeaderAndFooterPdfPageEventHelper.setTotalPages(getTotal());
        pdfWriter.setPageEvent(headerAndFooter);


        document.open();

        BaseFont titleFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
        Font headerFont = new Font(titleFont, 20, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font subTitleFont = new Font(Font.FontFamily.HELVETICA, 10, Font.ITALIC, new BaseColor(23, 15, 128));
        Font tableHead = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font data = new Font(Font.FontFamily.HELVETICA, 10);
        document.add(new Paragraph(Constant.TITLE, headerFont));
        document.add(new Paragraph(Constant.DENSITY_SUB_TITLE, subTitleFont));

        int NUM_COLUMNS = 3;


        PdfPTable dataTable = new PdfPTable(NUM_COLUMNS);
        dataTable.setWidthPercentage(100);
        dataTable.setWidths(new int[]{2, 1, 1});
        dataTable.getDefaultCell().setBorder(1);
        dataTable.setHeaderRows(1);

        PdfPCell[] datacells = new PdfPCell[NUM_COLUMNS];
        for (int i = 0; i < NUM_COLUMNS; i++) {
            datacells[i] = new PdfPCell(new Paragraph(Constant.DENSITY_COLUMN_HEADINGS[i], tableHead));
            datacells[i].setBorder(PdfPCell.BOTTOM);
            datacells[i].setBorderWidthBottom(2f);
            datacells[i].setBorderColor(new BaseColor(23, 12, 128));
            datacells[i].setPadding(8);
            if (i == 0)
                datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
            else
                datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);

            dataTable.addCell(datacells[i]);
        }

        for (Product product : productList) {
            datacells[0] = new PdfPCell(new Paragraph((product.getPrice() * product.getQuantity()) + "" + (product.getPrice() * product.getQuantity()), data));
            datacells[1] = new PdfPCell(new Paragraph(product.getName(), data));
            datacells[2] = new PdfPCell(new Paragraph(product.getPrice() + "", data));

            for (int i = 0; i < NUM_COLUMNS; i++) {
                datacells[i].setBorder(PdfPCell.BOTTOM);
                datacells[i].setPadding(8);
                if (i == 0)
                    datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
                else
                    datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);
                dataTable.addCell(datacells[i]);
            }


        }
        document.add(dataTable);
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }

    @Override
    public ByteArrayInputStream generateTearResistancePdf() throws DocumentException, IOException {
        List<Product> productList = repository.findByIdLessThan(500);

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, out);

        Rectangle rectangle = new Rectangle(800, 600);
        pdfWriter.setBoxSize("rectangle", rectangle);
        document.setPageSize(rectangle);

        //Set footer
        HeaderAndFooterPdfPageEventHelper headerAndFooter = new HeaderAndFooterPdfPageEventHelper();
        HeaderAndFooterPdfPageEventHelper.setTotalPages(getTotal());
        pdfWriter.setPageEvent(headerAndFooter);

        document.open();

        BaseFont titleFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
        Font headerFont = new Font(titleFont, 20, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font subTitleFont = new Font(Font.FontFamily.HELVETICA, 10, Font.ITALIC, new BaseColor(23, 15, 128));
        Font tableHead = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font data = new Font(Font.FontFamily.HELVETICA, 10);
        document.add(new Paragraph(Constant.TITLE, headerFont));
        document.add(new Paragraph(Constant.TEAR_RESISTANCE_SUB_TITLE, subTitleFont));

        int NUM_COLUMNS = 3;


        PdfPTable dataTable = new PdfPTable(NUM_COLUMNS);
        dataTable.setWidthPercentage(100);
        dataTable.setWidths(new int[]{2, 1, 1});
        dataTable.getDefaultCell().setBorder(1);
        dataTable.setHeaderRows(1);

        PdfPCell[] datacells = new PdfPCell[NUM_COLUMNS];
        for (int i = 0; i < NUM_COLUMNS; i++) {
            datacells[i] = new PdfPCell(new Paragraph(Constant.TEAR_RESISTANCE_COLUMN_HEADINGS[i], tableHead));
            datacells[i].setBorder(PdfPCell.BOTTOM);
            datacells[i].setBorderWidthBottom(2f);
            datacells[i].setBorderColor(new BaseColor(23, 12, 128));
            datacells[i].setPadding(8);
            if (i == 0)
                datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
            else
                datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);

            dataTable.addCell(datacells[i]);
        }

        for (Product product : productList) {
            datacells[0] = new PdfPCell(new Paragraph((product.getPrice() * product.getQuantity()) + "" + (product.getPrice() * product.getQuantity()), data));
            datacells[1] = new PdfPCell(new Paragraph(product.getName(), data));
            datacells[2] = new PdfPCell(new Paragraph(product.getPrice() + "", data));

            for (int i = 0; i < NUM_COLUMNS; i++) {
                datacells[i].setBorder(PdfPCell.BOTTOM);
                datacells[i].setPadding(8);
                if (i == 0)
                    datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
                else
                    datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);
                dataTable.addCell(datacells[i]);
            }


        }
        document.add(dataTable);
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }

    @Override
    public ByteArrayInputStream generateTensileNElongationPdf() throws DocumentException, IOException {
        List<Product> productList = repository.findByIdLessThan(500);

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, out);

        Rectangle rectangle = new Rectangle(800, 600);
        pdfWriter.setBoxSize("rectangle", rectangle);
        document.setPageSize(rectangle);

        //Set footer
        HeaderAndFooterPdfPageEventHelper headerAndFooter = new HeaderAndFooterPdfPageEventHelper();
        HeaderAndFooterPdfPageEventHelper.setTotalPages(getTotal());
        pdfWriter.setPageEvent(headerAndFooter);


        document.open();

        BaseFont titleFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
        Font headerFont = new Font(titleFont, 20, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font subTitleFont = new Font(Font.FontFamily.HELVETICA, 10, Font.ITALIC, new BaseColor(23, 15, 128));
        Font tableHead = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC, new BaseColor(23, 15, 128));
        Font data = new Font(Font.FontFamily.HELVETICA, 10);
        document.add(new Paragraph(Constant.TITLE, headerFont));
        document.add(new Paragraph(Constant.TENSILE_ELONGATION_SUB_TITLE, subTitleFont));

        int NUM_COLUMNS = 5;

        PdfPTable dataTable = new PdfPTable(NUM_COLUMNS);
        dataTable.setWidthPercentage(100);
        dataTable.setWidths(new int[]{2, 1, 1, 1, 1});
        dataTable.getDefaultCell().setBorder(1);
        dataTable.setHeaderRows(1);

        PdfPCell[] datacells = new PdfPCell[NUM_COLUMNS];
        for (int i = 0; i < NUM_COLUMNS; i++) {
            datacells[i] = new PdfPCell(new Paragraph(Constant.TENSILE_ELONGATION_COLUMN_HEADINGS[i], tableHead));
            datacells[i].setBorder(PdfPCell.BOTTOM);
            datacells[i].setBorderWidthBottom(2f);
            datacells[i].setBorderColor(new BaseColor(23, 12, 128));
            datacells[i].setPadding(8);
            if (i == 0)
                datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
            else
                datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);

            dataTable.addCell(datacells[i]);
        }

        for (Product product : productList) {
            datacells[0] = new PdfPCell(new Paragraph((product.getPrice() * product.getQuantity()) + "" + (product.getPrice() * product.getQuantity()), data));
            datacells[1] = new PdfPCell(new Paragraph(product.getName(), data));
            datacells[2] = new PdfPCell(new Paragraph(product.getPrice() + "", data));
            datacells[3] = new PdfPCell(new Paragraph(product.getName(), data));
            datacells[4] = new PdfPCell(new Paragraph(product.getPrice() + "", data));

            for (int i = 0; i < NUM_COLUMNS; i++) {
                datacells[i].setBorder(PdfPCell.BOTTOM);
                datacells[i].setPadding(8);
                if (i == 0)
                    datacells[i].setHorizontalAlignment(Element.ALIGN_LEFT);
                else
                    datacells[i].setHorizontalAlignment(Element.ALIGN_CENTER);
                dataTable.addCell(datacells[i]);
            }


        }
        document.add(dataTable);
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }


    public ByteArrayInputStream gen() throws DocumentException, IOException {

        PdfReader document = new PdfReader(generateTensileNElongationPdf());
        setTotal(document.getNumberOfPages());

        return generateTensileNElongationPdf();
    }
}
