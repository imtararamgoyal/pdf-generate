package com.pio.pdf.service;

import com.itextpdf.text.DocumentException;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Product service
 *
 * @auther programmers.io
 * Created By: TararamGoyal
 * Date: 15-11-2021
 * Time: 17:10
 * Project Name: pdf
 */
public interface ProductService {

    public ByteArrayInputStream generateCFDPdf() throws DocumentException, IOException;

    public ByteArrayInputStream generateIFDPdf() throws DocumentException, IOException;

    public ByteArrayInputStream generateDensityPdf() throws DocumentException, IOException;

    public ByteArrayInputStream generateTearResistancePdf() throws DocumentException, IOException;

    public ByteArrayInputStream generateTensileNElongationPdf() throws DocumentException, IOException;

    public ByteArrayInputStream gen() throws DocumentException, IOException;
}
