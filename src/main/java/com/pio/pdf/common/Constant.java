package com.pio.pdf.common;

/**
 * @auther programmers.io
 * Created By: TararamGoyal
 * Date: 19-11-2021
 * Time: 15:57
 * Project Name: pdf
 */
public class Constant {

    public static final String TITLE = "Foam Property Specifications";

    public static final String CFD_SUB_TITLE = "CFD Properties";
    public static final String IFD_SUB_TITLE = "IFD Properties";
    public static final String DENSITY_SUB_TITLE = "Density Properties";
    public static final String TEAR_RESISTANCE_SUB_TITLE = "Tear Resistance Properties";
    public static final String TENSILE_ELONGATION_SUB_TITLE = "Tensile & Elongation Properties";

    public static final String[] CFD_COLUMN_HEADINGS = {"Foam Type", "25% min", "25% max", "50% min", "50% max", "70% min", "70% max"};
    public static final String[] IFD_COLUMN_HEADINGS = {"Foam Type", "25% min", "25% max", "65% min", "65% max"};
    public static final String[] DENSITY_COLUMN_HEADINGS = {"Foam Type", "Density min", "Density max"};
    public static final String[] TEAR_RESISTANCE_COLUMN_HEADINGS = {"Foam Type", "TR (PPI) min", "TR (PPI) max"};
    public static final String[] TENSILE_ELONGATION_COLUMN_HEADINGS = {"Foam Type", "TS (PSI) min", "TS (PSI) max", "EL (%) min", "EL (%) max"};

    public static final String[] CFD_COLUMN_SUB_HEADINGS = {"", "CFD", "CFD", "CFD", "CFD", "CFD", "CFD"};
    public static final String[] IFD_COLUMN_SUB_HEADINGS = {"", "IFD", "IFD", "IFD", "IFD"};

}
