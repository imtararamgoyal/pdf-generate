package com.pio.pdf.repository;

import com.pio.pdf.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Product Repository
 *
 * @auther programmers.io
 * Created By: TararamGoyal
 * Date: 15-11-2021
 * Time: 17:09
 * Project Name: pdf
 */
public interface ProductRepository extends CrudRepository<Product, Integer> {

    List<Product> findByIdLessThan(int id);
}
