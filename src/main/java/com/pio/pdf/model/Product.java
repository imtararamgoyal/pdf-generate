package com.pio.pdf.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Product Data Model
 *
 * @auther programmers.io
 * Created By: TararamGoyal
 * Date: 15-11-2021
 * Time: 17:07
 * Project Name: pdf
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "PRODUCT_TBL")
public class Product {

    private static final long serialVersionUID = 1307525040224585678L;

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private int quantity;
    private long price;

    public Product(String name, int quantity, long price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }


}
