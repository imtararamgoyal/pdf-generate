package com.pio.pdf.helper;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @auther programmers.io
 * Created By: TararamGoyal
 * Date: 18-11-2021
 * Time: 12:43
 * Project Name: pdf
 */
public class HeaderAndFooterPdfPageEventHelper extends PdfPageEventHelper {
    public static int totalPages = 0;

    public static int getTotalPages() {
        return totalPages;
    }

    public static void setTotalPages(int totalPages) {
        HeaderAndFooterPdfPageEventHelper.totalPages = totalPages;
    }


    public void onEndPage(PdfWriter pdfWriter, Document document) {

        Rectangle rect = pdfWriter.getPageSize();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        formatter.applyPattern("EEEE, MMMM dd, yyyy");

        String timeStamp = formatter.format(date);
        String page = " Page " + pdfWriter.getPageNumber() + " of " + getTotalPages();

        BaseColor footerColor = new BaseColor(23, 15, 128);

        Paragraph paraTimeStamp = new Paragraph(new Chunk(timeStamp,
                FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLDITALIC, footerColor)));

        Paragraph paraPageNo = new Paragraph(new Chunk(page,
                FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLDITALIC, footerColor)));

        Font fontFooter = new Font();
        fontFooter.setSize(8);


        paraTimeStamp.setFont(fontFooter);
        paraPageNo.setFont(fontFooter);

        Phrase footerPhraseTimeStamp = new Phrase(paraTimeStamp);
        Phrase footerPhraseParaPageNo = new Phrase(paraPageNo);


        LineSeparator ls = new LineSeparator();
        ls.setLineColor(new BaseColor(192, 192, 192));
        ls.setLineWidth(2);
        ls.drawLine(pdfWriter.getDirectContent(), rect.getLeft() + 35, rect.getRight() - 35, rect.getBottom() + 35);

        ColumnText.showTextAligned(pdfWriter.getDirectContent(),
                Element.ALIGN_CENTER, footerPhraseTimeStamp,
                rect.getLeft() + 90, rect.getBottom() + 25, 0);

        ColumnText.showTextAligned(pdfWriter.getDirectContent(),
                Element.ALIGN_CENTER, footerPhraseParaPageNo,
                rect.getRight() - 80, rect.getBottom() + 25, 0);
    }
}
