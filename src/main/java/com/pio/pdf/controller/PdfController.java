package com.pio.pdf.controller;

import com.itextpdf.text.DocumentException;
import com.pio.pdf.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Pdf Controller
 *
 * @auther programmers.io
 * Created By: TararamGoyal
 * Date: 15-11-2021
 * Time: 17:48
 * Project Name: pdf
 */
@RestController
@RequestMapping(value = "/pdf")
public class PdfController {
    @Autowired
    ProductService productService;

    @GetMapping()
    public ResponseEntity<Object> getPdf() throws DocumentException, IOException {

        String fileName = "product.pdf";
        HttpHeaders headers = new HttpHeaders();
        ByteArrayInputStream bis = null;
        bis = productService.gen();


        headers.add("Content-Disposition", "inline; filename=\"" + fileName + "\"");

        if (bis.available() == 0)
            return new ResponseEntity<>("No data for report generation", HttpStatus.NOT_FOUND);
        else
            return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(new InputStreamResource(bis));
    }

}
